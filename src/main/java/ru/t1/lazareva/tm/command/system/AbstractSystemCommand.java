package ru.t1.lazareva.tm.command.system;

import ru.t1.lazareva.tm.api.service.ICommandService;
import ru.t1.lazareva.tm.command.AbstractCommand;
import ru.t1.lazareva.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}