package ru.t1.lazareva.tm.repository;

import ru.t1.lazareva.tm.api.repository.IProjectRepository;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;
import ru.t1.lazareva.tm.model.Project;

import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

}
